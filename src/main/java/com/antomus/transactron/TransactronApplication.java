package com.antomus.transactron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class TransactronApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactronApplication.class, args);
	}

}
