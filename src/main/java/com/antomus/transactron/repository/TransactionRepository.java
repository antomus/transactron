package com.antomus.transactron.repository;

import com.antomus.transactron.model.Transaction;

import java.util.List;

public interface TransactionRepository {
  List<Transaction> findAll();

  boolean deleteAll();

  Transaction add(Transaction transaction);
}
