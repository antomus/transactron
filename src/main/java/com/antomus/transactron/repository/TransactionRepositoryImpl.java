package com.antomus.transactron.repository;

import com.antomus.transactron.model.Transaction;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class TransactionRepositoryImpl implements TransactionRepository {

  private static ConcurrentHashMap<Instant, Transaction> storage = new ConcurrentHashMap<>();
  private final int PAST_TIME_LIMIT = 60;

  @Override
  public List<Transaction> findAll() {
    ArrayList<Transaction> transactions = new ArrayList<>(storage.values());
    ArrayList<Transaction> result = new ArrayList<>();


    for(Transaction transaction : transactions) {
      Instant transactionTimestamp = transaction.getTimestamp();
      // same time in millis
      Instant now = Instant.now();

      Duration duration = Duration.between( transactionTimestamp , now );
      long secondsElapsed = duration.getSeconds() ;

     if(secondsElapsed < PAST_TIME_LIMIT) {
       result.add(transaction);
     }
    }

    return result;
  }

  @Override
  public boolean deleteAll() {
    storage.clear();
    return storage.isEmpty();
  }

  @Override
  public Transaction add(Transaction transaction) {
    storage.put(transaction.getTimestamp(), transaction);
    return transaction;
  }


}
