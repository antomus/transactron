package com.antomus.transactron.errors;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.UNPROCESSABLE_ENTITY)
@ResponseBody
public class FutureTransactionException extends RuntimeException {

  public FutureTransactionException(String s) {

  }
}