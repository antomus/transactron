package com.antomus.transactron.errors;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NO_CONTENT)
@ResponseBody
public class OldTransactionException extends RuntimeException {

  public OldTransactionException(String s) {

  }
}
