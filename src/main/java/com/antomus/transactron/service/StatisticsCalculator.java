package com.antomus.transactron.service;

import com.antomus.transactron.model.StatisticsData;
import com.antomus.transactron.model.Transaction;
import com.antomus.transactron.repository.TransactionRepository;
import com.antomus.transactron.repository.TransactionRepositoryImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class StatisticsCalculator {

  private final TransactionService transactionService;

  public StatisticsCalculator(TransactionService transactionService) {
    this.transactionService = transactionService;
  }

  private List<Transaction> transactions;
  private final int scale = 2;
  private final int roundingMode = BigDecimal.ROUND_HALF_UP;

  public StatisticsData getData() {
    setTransactionData();
    return new StatisticsData(
      sum(),
      average(),
      max(),
      min(),
      getCount()
    );
  }

  private void setTransactionData() {
    transactions = transactionService.findAll();
  }

  private BigDecimal sum() {
    return transactions.stream()
      .map(a -> a.getAmount())
      .reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  private BigDecimal max() {
    Optional<BigDecimal> max =  transactions.stream()
      .map(a -> a.getAmount())
      .max(Comparator.naturalOrder());

    if (max.isPresent()) {
      return max.get();
    }

    return BigDecimal.ZERO;
  }

  private BigDecimal min() {
    Optional<BigDecimal> min =  transactions.stream()
      .map(a -> a.getAmount())
      .min(Comparator.naturalOrder());

    if (min.isPresent()) {
      return min.get();
    }

    return BigDecimal.ZERO;
  }

  private int getCount() {
    return transactions.size();
  }

  private BigDecimal average() {
    if(transactions.isEmpty())
      return BigDecimal.ZERO;

    return sum().divide(new BigDecimal(transactions.size()), scale, roundingMode);
  }
}
