package com.antomus.transactron.service;

import com.antomus.transactron.errors.FutureTransactionException;
import com.antomus.transactron.errors.OldTransactionException;
import com.antomus.transactron.model.Transaction;
import com.antomus.transactron.repository.TransactionRepository;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@CacheConfig(cacheNames={"transactions"})
public class TransactionService {

  private final long PAST_TIME_LIMIT = Duration.ofMinutes(1).getSeconds();
  private final long FUTURE_TIME_LIMIT = Duration.ofSeconds(20).getSeconds();


  private TransactionRepository repository;

  TransactionService(TransactionRepository repository) {
    this.repository = repository;
  }

  public Transaction add(Transaction transaction) {
    if(validateTransactionIsOld(transaction)) {
      throw new OldTransactionException("transaction time is " +transaction.getTimestamp().toString());
    }

    if(validateTransactionIsInFuture(transaction)) {
      throw new FutureTransactionException("transaction time is " +transaction.getTimestamp().toString());
    }

    return repository.add(transaction);
  }

  @CacheEvict(allEntries = true)
  public boolean deleteAll() {
    return repository.deleteAll();
  }

  @Cacheable
  List<Transaction> findAll() {
    return repository.findAll();
  }

  private boolean validateTransactionIsOld(Transaction transaction) {
    Instant transactionTimestamp = transaction.getTimestamp();
    // same time in millis
    Instant now = Instant.now();

    Duration duration = Duration.between( transactionTimestamp , now );
    long secondsElapsed = duration.getSeconds() ;

    return secondsElapsed > PAST_TIME_LIMIT;
  }

  private boolean validateTransactionIsInFuture(Transaction transaction) {
    Instant transactionTimestamp = transaction.getTimestamp();
    // same time in millis
    Instant now = Instant.now();


    Duration duration = Duration.between( now , transactionTimestamp );
    long secondsElapsed = duration.getSeconds() ;

    return secondsElapsed > FUTURE_TIME_LIMIT;
  }

}
