package com.antomus.transactron.controller;
import com.antomus.transactron.errors.FutureTransactionException;
import com.antomus.transactron.model.StatisticsData;
import com.antomus.transactron.model.Transaction;

import com.antomus.transactron.service.StatisticsCalculator;
import com.antomus.transactron.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TransactionController {
  @Autowired
  private TransactionService transactionService;


  @PostMapping("/transactions")
  ResponseEntity<String> create(@RequestBody Transaction transaction) {
    transactionService.add(transaction);
    return new ResponseEntity<>("", HttpStatus.CREATED);
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  public ResponseEntity<String> handleDefault() {
    return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(FutureTransactionException.class)
  public ResponseEntity<String> handleFutureTransactionException() {
    return new ResponseEntity<>("", HttpStatus.UNPROCESSABLE_ENTITY);
  }
  @DeleteMapping ("/transactions")
  ResponseEntity<String> delete() {
    transactionService.deleteAll();
    return new ResponseEntity<>("", HttpStatus.NO_CONTENT);
  }
}
