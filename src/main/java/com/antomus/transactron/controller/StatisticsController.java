package com.antomus.transactron.controller;

import com.antomus.transactron.model.StatisticsData;
import com.antomus.transactron.service.StatisticsCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticsController {

  @Autowired
  private StatisticsCalculator statisticsCalculator;

  @GetMapping("/statistics")
  StatisticsData index() {
    return statisticsCalculator.getData();
  }
}
