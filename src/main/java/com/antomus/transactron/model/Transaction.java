package com.antomus.transactron.model;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class Transaction {
  private Instant timestamp;
  private BigDecimal amount;

  public Instant getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Instant date) {
    this.timestamp = date;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Transaction(Instant timestamp, BigDecimal amount) {
    this.timestamp = timestamp;
    this.amount = amount;
  }

  public Transaction() {}

  public boolean equals(Object o) {
    if (!(o instanceof Transaction)) {
      return false;
    }
    Transaction other = (Transaction) o;

    return timestamp.equals(other.timestamp) && amount.equals(other.amount);
  }

}
