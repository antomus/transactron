package com.antomus.transactron.service;

import com.antomus.transactron.model.Transaction;
import com.antomus.transactron.repository.TransactionRepositoryImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@SpringBootTest
@AutoConfigureMockMvc

public class TransactionServiceTest {

  @InjectMocks
  private TransactionService service;

  @Mock
  private TransactionRepositoryImpl repository; // this will be injected into StatisticsCalculator

  @Before
  public void setUp(){
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void addTransaction() {
    Instant now = Instant.now();
    Transaction mockTrnsaction = new Transaction(
      now,
      new BigDecimal("100.21")
    );

    doReturn(mockTrnsaction).when(repository).add(any());
    Transaction returnedTransaction = service.add(mockTrnsaction);
    assertNotNull(returnedTransaction);
  }

  @Test
  public void deleteAllTransactions() {
    // when
    service.deleteAll();

    // then
    Mockito.verify(repository, times(1)).deleteAll();
  }

}
