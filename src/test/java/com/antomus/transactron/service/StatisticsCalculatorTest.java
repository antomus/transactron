package com.antomus.transactron.service;

import com.antomus.transactron.model.StatisticsData;
import com.antomus.transactron.model.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;

import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest


public class StatisticsCalculatorTest {
  @InjectMocks
  private StatisticsCalculator calculator;

  @Mock
  private TransactionService transactionService; // this will be injected into StatisticsCalculator

  @Before
  public void setUp(){
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void calculateWithoutTransactions() {
    StatisticsData data = new StatisticsData(
      BigDecimal.valueOf(0),
      BigDecimal.valueOf(0),
      BigDecimal.valueOf(0),
      BigDecimal.valueOf(0),
      0
    );

    assertEquals(data, calculator.getData());
  }

  @Test
  public void calculateWithTransactions() {
    Instant now = Instant.now();
    Transaction mockTrnsaction1 = new Transaction(
      now,
      new BigDecimal("100.21")
    );
    Transaction mockTrnsaction2 = new Transaction(
      now,
      new BigDecimal("100.21")
    );
    doReturn(Arrays.asList(mockTrnsaction1, mockTrnsaction2)).when(transactionService).findAll();
    //BigDecimal sum, BigDecimal avg, BigDecimal max, BigDecimal min, int count

    StatisticsData data = new StatisticsData(
      BigDecimal.valueOf(200.42),
      BigDecimal.valueOf(100.21),
      BigDecimal.valueOf(100.21),
      BigDecimal.valueOf(100.21),
      2
    );

    assertThat(data, samePropertyValuesAs(calculator.getData()));
  }
}
