package com.antomus.transactron.repository;

import com.antomus.transactron.model.Transaction;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.springframework.test.util.ReflectionTestUtils.getField;

@SpringBootTest

public class TransactionRepositoryImplTest {

  private TransactionRepositoryImpl repository;

  @Before
  public void setUp(){
    this.repository = new TransactionRepositoryImpl();
  }

  @After
  public void tearDown(){
    Map<Instant, Transaction> storage = (ConcurrentHashMap<Instant, Transaction>) ReflectionTestUtils.getField(this.repository, "storage");
    storage.clear();
  }

  @Test

  public void additionTest() {
    Instant now = Instant.now();
    Transaction mockTransaction = new Transaction(
      now,
      new BigDecimal("100.21")
    );

    repository.add(mockTransaction);

    Map<Instant, Transaction> storage = (ConcurrentHashMap<Instant, Transaction>) ReflectionTestUtils.getField(repository, "storage");
    assertEquals(now, storage.keySet().toArray()[0]);
    assertEquals(mockTransaction, storage.values().toArray()[0]);
  }

  @Test
  public void deletionTest() {
    Instant now = Instant.now();
    Transaction mockTransaction = new Transaction(
      now,
      new BigDecimal("100.21")
    );

    repository.add(mockTransaction);
    repository.deleteAll();


    Map<Instant, Transaction> storage = (ConcurrentHashMap<Instant, Transaction>) ReflectionTestUtils.getField(repository, "storage");
    assertTrue(storage.isEmpty());
  }

  @Test
  public void findAllTest() {
    Instant now = Instant.now();
    Transaction mockTransaction = new Transaction(
      now,
      new BigDecimal("100.21")
    );

    repository.add(mockTransaction);

    assertThat(mockTransaction, samePropertyValuesAs(repository.findAll().get(0)));
  }

  @Test
  public void findAllNegativeTest() {
    List<Transaction> result = repository.findAll();

    assertTrue(result.isEmpty());
  }
}
